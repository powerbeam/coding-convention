//=============================================================================
// PowerBeam LLC
// (C) Copyright 2017. All Rights Reserved.
//
//! \file       AClass.h
//!
//! \brief      a source file for sample class A
//!
//! \details
//! prefer cmake over platform specific build files (Makefile / .vcproj) <br>
//! try to use doxygen and try to put all doxygen doc in headers <br>
//! prefer lower case letters in comments <br>
//! use '*' for each line in a block comment <br>
//! use cammel case for names <br>
//! for variables, lower case first letter <br>
//! for types, upper case first letter <br>
//! for methods, upper case first letter <br>
//! for members, prefix with 'm_' <br>
//! for constants, prefix with 'k_' <br>
//! use all caps for entities with large scope and are not types <br>
//! use //==== for highlighting, try not to highlight everything <br>
//! try to contain definitions in namespace <br>
//! avoid 'using namespace ...' in header and with global scope <br>
//! use clang-format to clean up style: clang-format -style=WebKit *.cpp *.h <br>
//! use 4 spaces for intentation <br>
//!
//! \author     Tim Tan <tim.tan@powerbeaminc.com>
//!
//=============================================================================

#ifndef _ACLASS_H_
#define _ACLASS_H_

namespace ASpace {

//! enum for types of counters
enum CounterTypes {
    INVALID = 0,
    INCREMENTER,
    CIRCULAR,
    MAX,
};

//=============================================================================
/*!
 *  \class      AClass
 *
 *  \brief      a class that does things
 */
//=============================================================================
class AClass {
public:
    //! default constructor
    AClass();

    //! \brief sets count
    //! \param count The description of the argument
    //! \return true upon success; false upon failure
    bool SetCount(int count);

    //! a simple clearing doesn't need much explaning, so use 1 line
    void ClearCount();

    //! constant for maximum count
    static const int k_maxCnt = 10;

protected:
    //! total count
    int m_totalCnt;

private:
    //! secret count
    int m_mySecretCnt;
};
}

#endif // _ACLASS_H_
