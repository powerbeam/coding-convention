//=============================================================================
// PowerBeam LLC
// (C) Copyright 2017. All Rights Reserved.
//
//! \file       AClass.cpp
//!
//! \author     Tim Tan <tim.tan@powerbeaminc.com>
//!
//=============================================================================

#include "AClass.h"

namespace ASpace {
using namespace std;

AClass::AClass()
    : m_totalCnt(0)
    , m_mySecretCnt(0)
{
}

bool AClass::SetCount(int count)
{
    // don't use doxygen for details of implementation
    int myCount = count;
    m_mySecretCnt = myCount;
    m_totalCnt += 1;

    // always use { }, it comforts the reader
    if (myCount > 1) {
        myCount += 2;
    }

    return true;
}

void AClass::ClearCount() { m_totalCnt = 0; }
}
